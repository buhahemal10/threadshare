<?php

use App\Http\Controllers\Auth\AuthutilsController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\VerificationController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('email/verify/{id}', [VerificationController::class, 'verify'])->name('verification.verify'); // Make sure to keep this as your route name
Route::get('email/resend', [VerificationController::class, 'resend'])->name('verification.resend');
Route::group(['prefix' => 'accounts', 'as' => 'account.'], function () {
    Route::post('login', [LoginController::class, 'login'])->name('login');
    Route::post('create', [RegisterController::class, 'store'])->name('registration');
    Route::post('forgot-password', [VerificationController::class, 'forgotPassword'])->name('password.email');
    Route::post('reset-password', [AuthutilsController::class, 'resetPassword'])->name('password.update');
});
