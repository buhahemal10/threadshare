<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegistrationRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Response;

class RegisterController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegistrationRequest $RegistrationRequest)
    {
        $registredUser = User::create([
            'name' => $RegistrationRequest->name,
            'email' => $RegistrationRequest->email,
            'password' => $RegistrationRequest->password,
        ]);
        $registredUser->sendEmailVerificationNotification();
        return Response::json(['code' => HttpResponse::HTTP_CREATED, 'user' => $registredUser], HttpResponse::HTTP_CREATED);
    }
}
