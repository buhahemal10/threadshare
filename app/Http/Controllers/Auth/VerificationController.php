<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class VerificationController extends Controller
{
    public function verify($userId, Request $request)
    {
        if (!$request->hasValidSignature()) {
            return Response::json(['code' => 401, 'message' => 'Invalid/Expired url provided.'], 401);
        }
        $user = User::findOrFail($userId);
        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
        }
        return Response::json(['code' => 200, 'message' => 'Email successfully verified!']);
    }

    public function forgotPassword(ForgotPasswordRequest $forgotPasswordRequest)
    {
        $status = Password::sendResetLink($forgotPasswordRequest->only('email'));
        return Response::json(['code' => HttpResponse::HTTP_OK, 'passwordresetcode' => $status, 'message' => 'Password reset link sended to your mail']);
    }

    public function resend()
    {
        if (Auth::user()->hasVerifiedEmail()) {
            return Response::json(['code' => 400, 'message' => 'Email already verified.'], 400);
        }
        Auth::user()->sendEmailVerificationNotification();
        return Response::json(['code' => 200, 'message' => 'Email verification link sent on your email id'], 200);
    }
}
