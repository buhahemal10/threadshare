<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\Response as HttpResponse;

class LoginController extends Controller
{
    public function login(LoginRequest $loginRequest)
    {
        $credentials = (['email' => $loginRequest['email'], 'password' => $loginRequest['password']]);
        if (!Auth::attempt($credentials)) {
            return Response::json(['code' => HttpResponse::HTTP_FORBIDDEN, 'message' => 'The provided password is incorrect.']);
        }
        $user = User::where('email', $credentials['email'])->first();
        $token = $user->createToken($loginRequest['device_name'])->plainTextToken;
        return Response::json(['code' => HttpResponse::HTTP_OK, 'token' => $token, 'token_type' => 'Bearer']);
    }
}
