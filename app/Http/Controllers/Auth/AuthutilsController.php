<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Http\Response as HttpResponse;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Str;

class AuthutilsController extends Controller
{
    public function resetPassword(ResetPasswordRequest $resetPasswordRequest)
    {
        $status = Password::reset(
            $resetPasswordRequest->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) {
                $user->forceFill([
                    'password' => $password
                ])->setRememberToken(Str::random(60));
                $user->save();
                // event(new PasswordReset($user));
            }
        );
        return Response::json(['code' => HttpResponse::HTTP_OK, 'passwordresetcode' => $status, 'message' => 'Password reset done']);
    }
}
