<?php
namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;

class Setup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:dev';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Application Setup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('<fg=cyan>Wipe Database & Clear Cahce In Progress.....</>');
        Artisan::call('migrate:fresh');
        $this->info('<fg=yellow>' . Artisan::output() . '</>');
        $this->info('<fg=cyan>Migration Done</>');
        $this->info('<fg=cyan>-------**-------</>');
        $this->info('<fg=cyan>-------**-------</>');
        $this->info('<fg=cyan>Database Seeding Process Start</>');
        Artisan::call('db:seed');
        $this->info('<fg=yellow>' . Artisan::output() . '</>');
        $this->info('<fg=cyan>Database Seeding Done Hold On We are on way to setup cache</>');
        Artisan::call('optimize');
        Artisan::call('config:clear');
        Artisan::call('cache:clear');
        Artisan::call('route:cache');
        Artisan::call('view:cache');
        $this->info('<fg=green>Sucessfully Setup Application Ready to Go</>');
    }
}
