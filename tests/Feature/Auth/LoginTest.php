<?php
namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function testMustEnterEmailAndPasswordAndDevicename()
    {
        $this->json('POST', '/api/accounts/login')
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.',
                'errors' => [
                    'email' => ['The email field is required.'],
                    'password' => ['The password field is required.'],
                    'device_name' => ['The device name field is required.']
                ]
            ]);
    }

    public function testSuccessfullLogin()
    {
        $user = User::factory(1)->create();
        $loginCred = ['email' => $user[0]->email, 'password' => 'password', 'device_name' => 'unitTest'];
        $this->json('POST', '/api/accounts/login', $loginCred, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonStructure([
                'code',
                'token',
                'token_type'
            ]);
        $this->assertAuthenticated();
    }
}
